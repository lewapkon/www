from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'www.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    
    url(r'^login/$', 'django.contrib.auth.views.login',
    	{'template_name': 'workshops/login.html'}, name='login'),

    url(r'^logout/$', 'django.contrib.auth.views.logout',
    	name='logout'),

    url(r'', include('workshops.urls')),
)
