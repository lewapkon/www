from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from workshops import models

class PersonInline(admin.StackedInline):
	model = models.Person
	fk_name = 'user'
	max_num = 1

class CustomUserAdmin(UserAdmin):
	inlines = [PersonInline,]

# Register your models here.

admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
#admin.site.register(models.Person)
admin.site.register(models.Workshop)
admin.site.register(models.Exercise)
admin.site.register(models.Application)
admin.site.register(models.Solution)
