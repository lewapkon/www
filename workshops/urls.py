from django.conf.urls import patterns, url
from django.views.generic import ListView, DetailView, RedirectView
from workshops import models, views

urlpatterns = patterns('',
    # Workshop.
    url(r'^workshops/$',
        views.WorkshopListView.as_view(),
        name='workshops'),

    url(r'^workshop/$',
        views.WorkshopCreateView.as_view(),
        name='workshop_create'),

    url(r'^workshop/(?P<pk>\d+)/delete/$',
        views.WorkshopDeleteView.as_view(),
        name='workshop_delete'),

    url(r'^workshop/(?P<pk>\d+)/$',
        views.WorkshopDetailView.as_view(),
        name='workshop'),

    url(r'^workshop/(?P<pk>\d+)/edit/$',
        views.WorkshopUpdateView.as_view(),
        name='workshop_edit'),

    # Application.
    url(r'^workshop/(?P<workshop__pk>\d+)/apply/$',
        views.ApplicationCreateView.as_view(),
        name='apply'),

    url(r'^workshop/(?P<slug>\d+)/disapply/$',
        views.ApplicationDeleteView.as_view(),
        name='disapply'),

    url(r'^workshop/(?P<workshop__pk>\d+)/applications/$',
        views.ApplicationListView.as_view(),
        name='applications'),

    url(r'^workshop/(?P<workshop__pk>\d+)/application/(?P<pk>\d+)/$',
        views.ApplicationUpdateView.as_view(),
        name='application'),

    # Exercise.
    url(r'^workshop/(?P<workshop__pk>\d+)/newtask/$',
        views.ExerciseCreateView.as_view(),
        name='exercise_create'),

    url(r'^task/(?P<pk>\d+)/delete/$',
        views.ExerciseDeleteView.as_view(),
        name='exercise_delete'),

    url(r'^task/(?P<pk>\d+)/$',
        views.ExerciseDetailView.as_view(),
        name='exercise'),

    url(r'^task/(?P<pk>\d+)/edit/$',
        views.ExerciseUpdateView.as_view(),
        name='exercise_edit'),

    # Solution.
    url(r'task/(?P<exercise__pk>\d+)/solution/$',
        views.SolutionCreateUpdateView.as_view(),
        name='solution_update'),

    url(r'task/(?P<exercise__pk>\d+)/solution/(?P<pk>\d+)/$',
        views.SolutionGradeView.as_view(),
        name='solution_grade'),

    url(r'task/(?P<exercise__pk>\d+)/solutions/$',
        views.SolutionListView.as_view(),
        name='solutions'),

    # Person.
    url(r'^users/$',
        views.PersonListView.as_view(),
        name='people'),

    url(r'^user/(?P<slug>[a-zA-Z0-9-]+)/$',
        views.PersonDetailView.as_view(),
        name='person'),

    url(r'^$', RedirectView.as_view(
        url='/workshops'
    )),
)
