from django.db import models
from django.contrib.auth.models import User

# Create your models here.

shirtSizeChoices = ((0,'XXS'),(1,'XS'),(2,'S'),(3,'M'),
    (4,'L'),(5,'XL'),(6,'XXL'))

class Person(models.Model):
    user = models.OneToOneField(User)
    shirt_size = models.PositiveSmallIntegerField(
        max_length=3, choices=shirtSizeChoices, default=3)

    def __str__(self):
        if self.user.first_name and self.user.last_name:
            return self.user.first_name + ' ' + self.user.last_name
        else:
            return self.user.username

class Workshop(models.Model):
    name = models.CharField(max_length=30)
    host = models.ForeignKey(Person, related_name='workshop_host')
    text = models.TextField(max_length=5000)
    
    def __str__(self):
        return self.name

class Application(models.Model):
    person = models.ForeignKey(Person)
    workshop = models.ForeignKey(Workshop)
    qualified = models.BooleanField(default=False)

    def __str__(self):
        return '{0} - {1} ({2})'.format(
            self.person,
            self.workshop,
            self.qualified)

class Exercise(models.Model):
    name = models.CharField(max_length=30)
    text = models.TextField(max_length=3000)
    workshop = models.ForeignKey(Workshop)

    def __str__(self):
        return '{0} - {1}'.format(self.workshop, self.name)

class Solution(models.Model):
    application = models.ForeignKey(Application) #it already has a workshop in it
    exercise = models.ForeignKey(Exercise)
    text = models.TextField(max_length=5000)
    grade = models.PositiveSmallIntegerField(null=True, blank=True)

    def __str__(self):
        return '{0}: {1} - {2} ({3})'.format(
            self.application.workshop,
            self.exercise.name,
            self.application.person,
            self.grade)
