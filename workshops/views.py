from django import http
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, DetailView
from workshops import models

# Mixins.

class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

class OnlySelectedUsersMixin(LoginRequiredMixin):
    selectedUsers = tuple()

    def dispatch(self, request, *args, **kwargs):
        if hasattr(request.user, 'person') and (request.user.person in self.selectedUsers or kwargs.get('permit')):
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied

class OnlySelectedUsersOrSuperuserMixin(OnlySelectedUsersMixin):
    def dispatch(self, request, *args, **kwargs):
        try:
            return super().dispatch(request, *args, **kwargs)
        except PermissionDenied:
            if request.user.is_superuser:
                return super().dispatch(request, *args, permit=True, **kwargs)
            raise PermissionDenied

class OnlyNotSelectedUsersMixin(LoginRequiredMixin):
    selectedUsers = tuple()

    def dispatch(self, request, *args, **kwargs):
        if hasattr(request.user, 'person') and not request.user.person in self.selectedUsers:
            return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied

# Workshop.

class WorkshopListView(ListView):
    model = models.Workshop
    context_object_name = 'workshops_list'

class WorkshopCreateView(LoginRequiredMixin, CreateView):
    model = models.Workshop
    fields = ['name', 'text']

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.host = self.request.user.person
        obj.save()
        return http.HttpResponseRedirect(reverse_lazy('workshop', args=(obj.pk,)))

class WorkshopUpdateView(OnlySelectedUsersOrSuperuserMixin, UpdateView):
    model = models.Workshop
    fields = ['name', 'text']

    def dispatch(self, request, *args, **kwargs):
        workshop = models.Workshop.objects.get(pk=self.kwargs['pk'])
        self.selectedUsers = (workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('workshop', args=(self.kwargs['pk'],))

class WorkshopDeleteView(OnlySelectedUsersOrSuperuserMixin, DeleteView):
    model = models.Workshop
    success_url = reverse_lazy('workshops')

    def dispatch(self, request, *args, **kwargs):
        workshop = models.Workshop.objects.get(pk=self.kwargs['pk'])
        self.selectedUsers = (workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['workshop'] = self.object
        return context

class WorkshopDetailView(DetailView):
    model = models.Workshop
    context_object_name = 'workshop'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated():
            currentUser = self.request.user.person
            objectOwner = self.object.host
            try:
                application = models.Application.objects.get(
                    workshop=self.object,person=currentUser)
            except models.Application.DoesNotExist:
                application = None
            context['isOwner'] = currentUser == objectOwner
            context['application'] = application

        context['applications'] = models.Application.objects.filter(workshop=self.object).order_by('-qualified')
        context['exercises'] = models.Exercise.objects.filter(workshop=self.object)
        return context

# Application.

class ApplicationCreateView(OnlyNotSelectedUsersMixin, CreateView):
    model = models.Application
    fields = []

    def dispatch(self, request, *args, **kwargs):
        if hasattr(request.user, 'person'):
            workshop = models.Workshop.objects.get(pk=self.kwargs['workshop__pk'])
            self.selectedUsers = (workshop.host,)
            currentUser = request.user.person
            
            try:
                application = models.Application.objects.get(
                    workshop=workshop,
                    person=currentUser)
            except models.Application.DoesNotExist:
                application = None

            if not application:
                return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['workshop'] = models.Workshop.objects.get(pk=self.kwargs['workshop__pk'])
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.workshop = models.Workshop.objects.get(
            pk=self.kwargs['workshop__pk'])
        obj.person = self.request.user.person
        obj.save()
        return http.HttpResponseRedirect(reverse_lazy('workshop', args=(self.kwargs['workshop__pk'],)))

class ApplicationDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Application
    slug_field = 'workshop__pk'

    def dispatch(self, request, *args, **kwargs):
        if self.get_object():
            return super().dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied

    def get_success_url(self):
        return reverse_lazy('workshop', args=(self.kwargs['slug'],))

    def get_object(self):
        if not hasattr(self, '_application'):
            try:
                self._application = models.Application.objects.get(
                    workshop__pk=self.kwargs['slug'],
                    person=self.request.user.person)
            except models.Application.DoesNotExist:
                self._application = None
        return self._application

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['workshop'] = models.Workshop.objects.get(pk=self.kwargs['slug'])
        return context

class ApplicationListView(OnlySelectedUsersOrSuperuserMixin, ListView):
    model = models.Application
    context_object_name = 'applications_list'

    def dispatch(self, request, *args, **kwargs):
        self._workshop = models.Workshop.objects.get(pk=kwargs['workshop__pk'])
        self.selectedUsers = (self._workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        try:
            queryset = models.Application.objects.filter(workshop__pk=self.kwargs['workshop__pk']).order_by('-qualified')
        except models.Solution.DoesNotExist:
            queryset = None
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['workshop'] = self._workshop
        return context

class ApplicationUpdateView(OnlySelectedUsersOrSuperuserMixin, UpdateView):
    model = models.Application
    fields = ['qualified']
    context_object_name = 'application'
    template_name = 'workshops/application_detail.html'

    def dispatch(self, request, *args, **kwargs):
        self._workshop = models.Workshop.objects.get(pk=kwargs['workshop__pk'])
        application = models.Application.objects.get(pk=kwargs['pk'])
        self.selectedUsers = (self._workshop.host, application.person)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['solutions_list'] = models.Solution.objects.filter(application__pk=self.kwargs['pk'])
        except models.Solution.DoesNotExist:
            context['solutions_list'] = None
        return context

    def get_success_url(self):
        return reverse_lazy('applications', args=(self._workshop.pk,))

# Exercise.

class ExerciseCreateView(OnlySelectedUsersOrSuperuserMixin, CreateView):
    model = models.Exercise
    fields = ['name', 'text']

    def dispatch(self, request, *args, **kwargs):
        self._workshop = models.Workshop.objects.get(pk=self.kwargs['workshop__pk'])
        self.selectedUsers = (self._workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['workshop'] = self._workshop
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.workshop = models.Workshop.objects.get(
            pk=self.kwargs['workshop__pk'])
        obj.save()
        return http.HttpResponseRedirect(reverse_lazy('exercise', args=(obj.pk,)))

class ExerciseUpdateView(OnlySelectedUsersOrSuperuserMixin, UpdateView):
    model = models.Exercise
    fields = ['name', 'text']

    def dispatch(self, request, *args, **kwargs):
        self._exercise = models.Exercise.objects.get(pk=self.kwargs['pk'])
        self.selectedUsers = (self._exercise.workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['workshop'] = self._exercise.workshop
        return context

    def get_success_url(self):
        return reverse_lazy('exercise', args=(self.kwargs['pk'],))

class ExerciseDeleteView(OnlySelectedUsersOrSuperuserMixin, DeleteView):
    model = models.Exercise

    def dispatch(self, request, *args, **kwargs):
        self._workshop = models.Exercise.objects.get(pk=kwargs['pk']).workshop
        self.selectedUsers = (self._workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('workshop', args=(self._workshop.pk,))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['workshop'] = self._workshop
        return context

class ExerciseDetailView(DetailView):
    model = models.Exercise

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['workshop'] = self.object.workshop

        if hasattr(self.request.user, 'person'):
            context['isOwner'] = self.request.user.person == self.object.workshop.host
        try:
            context['application'] = models.Application.objects.get(
                workshop=self.object.workshop,
                person=self.request.user.person)
        except:
            context['application'] = None
        try:
            context['solution'] = models.Solution.objects.get(
                application=application,
                exercise=self.object)
        except:
            context['solution'] = None
        return context

# Person.

class PersonListView(ListView):
    model = models.Person
    context_object_name = 'people_list'

class PersonDetailView(DetailView):
    model = models.Person
    slug_field = 'user__username'
    context_object_name = 'person'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # gets workshops hosted by the user
        person = models.Person.objects.get(user__username=self.kwargs['slug'])
        context['host_workshops'] = models.Workshop.objects.filter(host=person)
        # gets applications which the user participates in
        context['applications'] = models.Application.objects.filter(
            person=person).order_by('-qualified') # qualified are first
        return context

# Solution.

class SolutionCreateUpdateView(OnlyNotSelectedUsersMixin, UpdateView):
    model = models.Solution
    fields = ['text']

    def dispatch(self, request, *args, **kwargs):
        self.selectedUsers = (self._get_exercise().workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['exercise'] = self._get_exercise()
        context['application'] = self._get_application()
        return context

    def _get_exercise(self):
        if not hasattr(self, '_exercise'):
            try:
                self._exercise = models.Exercise.objects.get(pk=self.kwargs['exercise__pk'])
            except models.Exercise.DoesNotExist:
                raise PermissionDenied
        return self._exercise

    def _get_application(self):
        if not hasattr(self, '_application'):
            try:
                self._application = models.Application.objects.get(
                    workshop=self._get_exercise().workshop,
                    person=self.request.user.person)
            except models.Application.DoesNotExist:
                raise PermissionDenied
        return self._application

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            obj = queryset.get(
                exercise=self._get_exercise(),
                application=self._get_application())
        except ObjectDoesNotExist:
            obj = None
        return obj

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.exercise = self._get_exercise()
        obj.application = self._get_application()
        obj.save()
        return http.HttpResponseRedirect(reverse_lazy('exercise', args=(obj.exercise.pk,)))

class SolutionListView(OnlySelectedUsersOrSuperuserMixin, ListView):
    model = models.Solution
    context_object_name = 'solutions_list'

    def dispatch(self, request, *args, **kwargs):
        workshop = models.Exercise.objects.get(pk=kwargs['exercise__pk']).workshop
        self.selectedUsers = (workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        try:
            queryset = models.Solution.objects.filter(exercise__pk=self.kwargs['exercise__pk']).order_by('-grade')
        except models.Solution.DoesNotExist:
            queryset = None
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['exercise'] = models.Exercise.objects.get(pk=self.kwargs['exercise__pk'])
        return context

class SolutionGradeView(OnlySelectedUsersOrSuperuserMixin, UpdateView):
    model = models.Solution
    fields = ['grade']
    template_name = 'workshops/solution_grade.html'

    def dispatch(self, request, *args, **kwargs):
        workshop = self._get_exercise().workshop
        self.selectedUsers = (workshop.host,)
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('solutions', args=(self.kwargs['exercise__pk'],))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['exercise'] = self._get_exercise()
        context['application'] = self._get_application()
        return context

    def _get_exercise(self):
        if not hasattr(self, '_exercise') or self._exercise is None:
            try:
                self._exercise = self.get_object().exercise
            except:
                self._exercise = None
        return self._exercise

    def _get_application(self):
        if not hasattr(self, '_application') or self._application is None:
            try:
                self._application = self.get_object().application
            except:
                self._application = None
        return self._application

    def get_object(self, queryset=None):
        if not hasattr(self, '_obj'):
            if queryset is None:
                queryset = self.get_queryset()
            try:
                self._obj = queryset.get(pk=self.kwargs['pk'])
            except ObjectDoesNotExist:
                self._obj = None
        return self._obj
